#/usr/bin/python3

import aggregator
import textprocessor
from sitevisitor.biomolecula import BiomoleculaVisitor

aggr = aggregator.Aggregator(textprocessor.TextProcessor(["драг-дизайн"]))
aggr.add_visitor_class(BiomoleculaVisitor, 2116)
aggr.process_all_sites()
for text in aggr.contains_keyword["драг-дизайн"]:
    print(text.identifier)

