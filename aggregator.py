#!/usr/bin/python3


class Aggregator:
    """Base class for the project. Nothing interestring.
    """
    def __init__(self, processor):
        """ In the nearest future there will be a config loaded right here.
        """
        self.processor = processor
        self.visitors = []

    def add_visitor_class(self, visitor_class, *args, **kwargs):
        self.visitors.append(visitor_class(self.processor, *args, **kwargs))

    def process_all_sites(self):
        self.texts = []
        self.contains_keyword = dict()
        for keyword in self.processor.wordlist:
            self.contains_keyword[keyword] = []
        for visitor in self.visitors:
            for text in visitor.texts():
                self.texts.append(text)
                for keyword in text.keywords:
                    self.contains_keyword[keyword].append(text)
