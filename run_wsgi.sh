#!/bin/bash

uwsgi --http-socket :9090 --wsgi-file wsgi/server.py | tee wsgi.log
