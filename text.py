#!/usr/bin/python3

class Text:
    """Some processed text from some biomedical site.
    
    Now we store here the identifier of the text, the URI for it and list of
    keywords that were found in the text.
    """

    def __init__(self, identifier, uri, keywords):
        self.identifier = identifier
        self.uri = uri
        self.keywords = keywords
