#!/usr/bin/python3

from sitevisitor.visitor import SiteVisitor
import sitevisitor.helpers as helpers


class BiomoleculaVisitor(SiteVisitor):
    """biomolecula.ru (биомолекула.ру) site visitor.

    Fortunately, the text URIs on this site are quite simple:
    biomolecula.ru/content/{id}

    HTML texts in the site are disgusting. Quod scripsi, scripsi.
    However, we can assume that there is <p class=date> somewhere, the line
    before contains the header and this and next lines contains the meaningful
    content which ends with <a href="/authors/"> (and so on). Suppose it would
    be better if the author will be included in the text.
    """
    def __init__(self, processor, last_id):
        SiteVisitor.__init__(self, processor)
        # Searching for the last text is not implemented yet.
        self.last_id = last_id 

    def new_texts(self):
        id = 15 # First fourteen texts are either empty or deleted.
        while id <= self.last_id:
            text = self.get_text_from_uri(id, self.uri_for_id(id))
            if text is not None:
                yield text
            id += 1

    def uri_for_id(self, id):
        return "http://biomolecula.ru/content/{}".format(id)

    def get_text_from_uri(self, id, uri):
        date_line = "<p class=date>"
        author_line = "Автор: <a href=\"/authors/"
        
        text = helpers.get_text(uri)
        lines = text.split('\n')

        l = -1
        for i in range(len(lines)):
            if lines[i].find(date_line) != -1:
                l = i
        if l == -1:
            return None

        r = -1
        for i in range(len(lines)):
            if lines[i].find(author_line) != -1:
                r = i
                rt = lines[i].find("</a>", lines[i].find(author_line))
        if r == -1:
            return None
        lines[r] = lines[r][:rt]

        header = helpers.no_html(lines[l - 1]).replace('  ', ' ')
        header = header.lstrip().rstrip()
        content = helpers.no_html("\n".join(lines[l:r + 1]))

        identifier = "biomolecula-{}: {}".format(id, header)
        
        return self.processor(identifier, uri, content)
