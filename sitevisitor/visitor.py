#!/usr/bin/python3

class SiteVisitor:
    """Base class for site visitors. We all respect OOP principles.
    
    It is created mostly for future features, e.g. loading some old data from
    a database file. So it will be required to implement the sync function in 
    new visitors.

    Now it is required to implement the new_texts() function. This should 
    yield the new texts or at least return something iteratable.

    old_texts is now crutched and stays here for future compatibility.
    """
    def __init__(self, text_processor):
        self.processor = text_processor
        self.old_texts = []

    def texts(self):
        for old_text in self.old_texts:
            yield old_text
        for new_text in self.new_texts():
            yield new_text
