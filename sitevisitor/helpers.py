#!/usr/bin/python3

"""Common helpers for visitors
"""

import requests


def get_text(uri):
    r = requests.get(uri)
    # There must be an exception, but...
    assert r.status_code == 200
    return r.text.replace('\r', '') # dos2unix


def normalize_spaces(s):
    res = [' ']
    for c in s:
        if res[-1] != ' ' or c != ' ':
            res.append(c)
    return ''.join(res).lstrip().rstrip()


def no_html(s):
    s = s.replace('<br>', '\n')
    res = []
    open = False
    for c in s:
        if c == '<':
            open = True
        elif c == '>':
            open = False
            res.append(' ')
        elif not open:
            res.append(c)
    return normalize_spaces(''.join(res).replace("&nbsp;", " "))


