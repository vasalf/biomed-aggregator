#!/usr/bin/python3

import copy
from text import Text


class TextProcessor:
    """Processor for the texts. Takes list of words to be searched in the text 
    in the constructor and text in __call__. Returns the Text object to be 
    stored in the Aggregator.
    """
    def __init__(self, wordlist):
        # The list cannot be changed in runtime
        self.wordlist = copy.copy(wordlist)
        self.punctuation_marks = [',', '.', '!', '?', '—']

    def __call__(self, identifier, uri, text):
        punctuated_words = text.split()
        words = []
        for punctuated_word in punctuated_words:
            word = punctuated_word
            for sign in self.punctuation_marks:
                word = word.replace(sign, '')
            word = word.lower()
            if word != '':
                words.append(word)
        wordset = set(words)
        contains = []
        for word in self.wordlist:
            if word in wordset:
                contains.append(word)
        return Text(identifier, uri, contains)
            
