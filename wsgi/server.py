#!/usr/bin/python3

import sys
sys.path.append("..")

import cgi
from aggregator import Aggregator
from textprocessor import TextProcessor
from sitevisitor.biomolecula import BiomoleculaVisitor
from time import time


def decorate_link(uri, text):
    return "  <p><a href=\"{}\">{}</a></p>\n".format(uri, text)


def application(env, start_response):
    beg_time = time()
    
    start_response('200 OK', [('Content-Type','text/html')])
    
    yield str.encode(open("wsgi/head.html", "r").read())

    post_env = env.copy()
    post_env['QUERY_STRING'] = ''
    resp = cgi.FieldStorage(
        fp=env['wsgi.input'],
        environ=post_env,
        keep_blank_values=True
    )

    query = resp.getvalue("query", None)
    if query is not None:
        yield str.encode(
            "  <p>The query was \"{}\".</p>\n".format(query))
        aggr = Aggregator(TextProcessor([query]))
        aggr.add_visitor_class(BiomoleculaVisitor, 100)
        aggr.process_all_sites()
        for text in aggr.contains_keyword[query]:
            yield str.encode(decorate_link(text.uri, text.identifier))

    yield str.encode(
        "  <p>Page generated in {} seconds.</p>\n".format(time() - beg_time))
            
    yield str.encode(open("wsgi/tail.html", "r").read())
    
        
        
